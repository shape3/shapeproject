/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapeproject;

/**
 *
 * @author acer
 */
public class Triangle {
    private double n1;
    private double n2;
    public static final double E = 0.5;
    public Triangle(double n1 ,double n2){
        this.n1 = n1;
        this.n2 = n2;
    }
    public double calArea(){
        return E * n1 * n2;
    }
    public double getn1(){
        return n1;
    }
    public double getn2(){
        return n2;
    }
    public void setP(double n1 , double n2){
        if (n1 <= 0 || n2 <= 0) {
            System.out.println("Error : Radius must more than zero!");
            return;
        }
        this.n1 = n1;
        this.n2 = n2;
    }
}
