/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapeproject;

/**
 *
 * @author acer
 */
public class Square {
    private double num;
    public Square(double num){
        this.num = num;
    }
    public double calArea(){
        return num*num;
    }
    public double getnum(){
        return num;
    }
    public void setnum(double num){
        if (num <= 0){
            System.out.println("Error : Radius must more than zero!");
            return;
        }
        this.num = num;
    }
    
}
