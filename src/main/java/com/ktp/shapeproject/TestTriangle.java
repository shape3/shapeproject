/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapeproject;

/**
 *
 * @author acer
 */
public class TestTriangle {
    public static void main(String[] args) {
        
        Triangle triangle1 = new Triangle(5,10);
        System.out.println("Area of Triangle(r = " + triangle1.getn1() + "," + triangle1.getn2() + ") is " + triangle1.calArea());
        triangle1.setP(4,8);
        System.out.println("Area of Triangle(r = " + triangle1.getn1() + "," + triangle1.getn2() + ") is " + triangle1.calArea());
        triangle1.setP(0, 0);
        System.out.println("Area of Triangle(r = " + triangle1.getn1() + "," + triangle1.getn2() + ") is " + triangle1.calArea());
    }
}
