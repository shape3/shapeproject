/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapeproject;

/**
 *
 * @author acer
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(5,4);
        System.out.println("Area of Rectangle1(r = " + rectangle1.getn1() + "," + rectangle1.getn2() + ") is " + rectangle1.calArea());
        rectangle1.setR(8,4);
        System.out.println("Area of Rectangle1(r = " + rectangle1.getn1() + "," + rectangle1.getn2() + ") is " + rectangle1.calArea());
        rectangle1.setR(0,0);
        System.out.println("Area of Rectangle1(r = " + rectangle1.getn1() + "," + rectangle1.getn2() + ") is " + rectangle1.calArea());
        
    }
}
