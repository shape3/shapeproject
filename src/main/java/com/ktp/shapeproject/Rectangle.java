/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapeproject;

/**
 *
 * @author acer
 */
public class Rectangle {
    private double n1;
    private double n2;
    
    public Rectangle(double n1 ,double n2){
        this.n1 = n1;
        this.n2 = n2;
    }
    public double calArea(){
        return n1*n2;
    }
    public double getn1(){
        return n1;
    }
    public double getn2(){
        return n2;
    }
    public void setR(double n1 , double n2){
        if (n1 <= 0 && n2 <= 0){
            System.out.println("Error : Radius must more than zero!");
            return;
        }
        this.n1 = n1;
        this.n2 = n2;
    }
}
